#include "heap.hpp"

#include "util.hpp"
#include "types.hpp"

#include <map>

void RTO::take(void){
    refcount++;
}

RTO::Type RTONull::getType(void) const{
    return Type::Null;
}
RTO::Type RTOBoolean::getType(void) const{
    return Type::Boolean;
}
RTO::Type RTOInteger::getType(void) const{
    return Type::Integer;
}

RTO& Heap::getObject(fml_heap_index pointer){
    auto it = objects.find(pointer);
    MY_ASSERT(it != objects.end());
    return *it->second;
}
fml_heap_index Heap::addObject(RTO& new_object){
    objects.emplace(next_pointer, &new_object);
    return next_pointer++;
}
void Heap::removeObject(fml_heap_index pointer){
    MY_ASSERT(objects.erase(pointer) == 1);
}


void Heap::take(fml_heap_index pointer){
    getObject(pointer).take();
}
void Heap::release(fml_heap_index pointer){
    RTO& rto = getObject(pointer);
    rto.refcount--;
    if(rto.refcount == 0){
        removeObject(pointer);
        delete &rto;
    }
}

void Heap::print(void){
    std::cout << "Heap state:" << std::endl;
    for(auto it : objects){
        std::cout << it.first << ": " << it.second->refcount << std::endl;
    }
}
