#include "globals.hpp"

#include "util.hpp"
#include "types.hpp"

#include <map>

Globals::~Globals(void){
    cleanup();
}
void Globals::cleanup(void){
    for(auto it : slots)
        heap.release(it.second);
    slots.clear();
}

void Globals::addSlot(fml_cp_index name, fml_heap_index rto){
    auto it = slots.find(name);
    if(it != slots.end()){
        fml_heap_index old_rto = it->second;
        heap.release(old_rto);
        it->second = rto;
    }else
        slots.emplace(name, rto);
    heap.take(rto);
}
void Globals::addMethod(fml_cp_index name, fml_cp_index method){
    MY_ASSERT(methods.emplace(name, method).second);
}
fml_heap_index Globals::getSlot(fml_cp_index name){
    auto it = slots.find(name);
    MY_ASSERT(it != slots.end());
    heap.take(it->second);
    return it->second;
}
fml_cp_index Globals::getMethod(fml_cp_index name){
    auto it = methods.find(name);
    MY_ASSERT(it != methods.end());
    return it->second;
}
