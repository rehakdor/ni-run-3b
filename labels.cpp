#include "labels.hpp"

#include "types.hpp"
#include "util.hpp"

#include <map>

void Labels::addLabel(fml_cp_index name, fml_ip address){
    labels.emplace(name, address);
}
fml_ip Labels::getLabel(fml_cp_index name){
    auto it = labels.find(name);
    MY_ASSERT(it != labels.end());
    return it->second;
}
