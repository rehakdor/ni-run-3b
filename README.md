Dependencies:
* C++11 compiler
  * Default uses clang, change clang++ to g++ in Makefile if you use GCC

All your bytecode testfiles are in /tests. I added test2.sh, which runs the binary on all .bc files.

In case of emergency:
If the VM crashes in your test cases, try commenting heap.cpp:42-45 (it's an object reference counter). It will memleak but continue to compute.
The reference counter works in all your bytecode testfiles, but memory management can be tricky.
Again, this is only if you encounter this hypothetical problem.
