#include "vm.hpp"

#include "exception.hpp"
#include "util.hpp"
#include "constant_pool.hpp"
#include "heap.hpp"
#include "operand_stack.hpp"
#include "code_vector.hpp"
#include "frame_stack.hpp"
#include "globals.hpp"
#include "labels.hpp"
#include "types.hpp"

#include <fstream>
#include <cstdint>
#include <memory>
#include <iostream>
#include <cstring>
#include <algorithm>

enum Tags{
    Integer = 0x00,
    Boolean = 0x06,
    Null = 0x01,
    String = 0x02,
    Slot = 0x04,
    Method = 0x03
};

enum Opcodes{
    Literal = 0x01,
    Print = 0x02,
    Drop = 0x10,
    GetGlobal = 0x0C,
    SetGlobal = 0x0B,
    Label = 0x00,
    Jump = 0x0E,
    Branch = 0x0D,
    CallFunction = 0x08,
    Return = 0x0F,
    GetLocal = 0x0A,
    SetLocal = 0x09
};

#define opcodeArgSize_entry(name, size) \
case Opcodes::name: \
    return size
size_t VM::opcodeArgSize(uint8_t opcode){
    switch(opcode){
        opcodeArgSize_entry(Literal, 2);
        opcodeArgSize_entry(Print, 3);
        opcodeArgSize_entry(Drop, 0);
        opcodeArgSize_entry(GetGlobal, 2);
        opcodeArgSize_entry(SetGlobal, 2);
        opcodeArgSize_entry(Label, 2);
        opcodeArgSize_entry(Jump, 2);
        opcodeArgSize_entry(Branch, 2);
        opcodeArgSize_entry(CallFunction, 3);
        opcodeArgSize_entry(Return, 0);
        opcodeArgSize_entry(GetLocal, 2);
        opcodeArgSize_entry(SetLocal, 2);
    }
    throw InvalidBytecode("Unknown opcode size: " + std::to_string(opcode));
}
CPOInteger *VM::loadCPOInteger(std::ifstream& file){
    fml_int value;
    file.read((char *)&value, sizeof(value));
    value = LE2NATIVE32(value);
    return new CPOInteger(value);
}
CPOBoolean *VM::loadCPOBoolean(std::ifstream& file){
    uint8_t value;
    file.read((char *)&value, sizeof(value));
    return new CPOBoolean(value);
}
CPOString *VM::loadCPOString(std::ifstream& file){
    uint32_t length;
    file.read((char *)&length, sizeof(length));
    length = LE2NATIVE32(length);
    std::unique_ptr<char[]> data(new char[length]);
    file.read(data.get(), length);
    return new CPOString(std::string(data.get(), length));
}
CPOSlot *VM::loadCPOSlot(std::ifstream& file){
    fml_cp_index index;
    file.read((char *)&index, sizeof(index));
    index = LE2NATIVE16(index);
    return new CPOSlot(index);
}
CPOMethod *VM::loadCPOMethod(std::ifstream& file){
    fml_cp_index name_index;
    uint8_t argument_count;
    fml_local_index local_count;
    uint32_t instruction_count;

    file.read((char *)&name_index, sizeof(name_index));
    name_index = LE2NATIVE16(name_index);
    file.read((char *)&argument_count, sizeof(argument_count));
    file.read((char *)&local_count, sizeof(local_count));
    local_count = LE2NATIVE16(local_count);
    file.read((char *)&instruction_count, sizeof(instruction_count));
    instruction_count = LE2NATIVE32(instruction_count);

    // compute code length
    size_t code_vector_start = code_vector.size();
    std::streampos file_start = file.tellg();
    for(size_t i = 0; i < instruction_count; i++){
        uint8_t opcode;
        file.read((char *)&opcode, sizeof(opcode));
        // register labels
        if(opcode == Opcodes::Label){
            fml_cp_index name_index;
            file.read((char *)&name_index, sizeof(name_index));
            name_index = LE2NATIVE16(name_index);
            labels.addLabel(name_index, code_vector_start + file.tellg() - file_start);
        }else
            file.seekg(opcodeArgSize(opcode), std::ios_base::cur);
    }

    // insert into code vector
    size_t length = file.tellg() - file_start;
    file.seekg(file_start, std::ios_base::beg);
    code_vector.extend(length);
    file.read((char *)code_vector.getWritable(code_vector_start), length);

    return new CPOMethod(name_index, argument_count, local_count, code_vector_start, length);
}
#define loadCPO_entry(arg) \
case Tags::arg: \
    return loadCPO##arg(file)
CPO *VM::loadCPO(std::ifstream& file){
    uint8_t tag;
    file.read((char *)&tag, sizeof(tag));
    switch(tag){
        loadCPO_entry(Integer);
        loadCPO_entry(Boolean);
        loadCPO_entry(String);
        loadCPO_entry(Slot);
        loadCPO_entry(Method);
        case Tags::Null:
            return new CPONull;
    }
    throw InvalidBytecode("Unknown tag");
}
void VM::loadConstantPool(std::ifstream& file){
    uint16_t cp_size;
    file.read((char *)&cp_size, sizeof(cp_size));
    cp_size = LE2NATIVE16(cp_size);
    for(size_t i = 0; i < cp_size; i++)
        constant_pool.addObject(*loadCPO(file));
}
void VM::loadGlobals(std::ifstream& file){
    uint16_t gl_size;
    file.read((char *)&gl_size, sizeof(gl_size));
    gl_size = LE2NATIVE16(gl_size);
    for(size_t i = 0; i < gl_size; i++){
        fml_cp_index target;
        file.read((char *)&target, sizeof(target));
        target = LE2NATIVE16(target);
        const CPO& cpo_target = constant_pool.getObject(target);
        if(cpo_target.getType() == CPO::Type::Method){
            const CPOMethod& cpo_method = static_cast<const CPOMethod&>(cpo_target);
            globals.addMethod(cpo_method.name_index, target);
        } // ignore Slot
    }
}
void VM::loadEntry(std::ifstream& file){
    fml_cp_index method;
    file.read((char *)&method, sizeof(method));
    method = LE2NATIVE16(method);
    entry = method;
}



unsigned int VM::CPO2RTO(const CPO& cpo){
    switch(cpo.getType()){
        case CPO::Type::Null:
            return heap.addObject(*new RTONull);
        case CPO::Type::Boolean:{
            const CPOBoolean& real_cpo = static_cast<const CPOBoolean&>(cpo);
            return heap.addObject(*new RTOBoolean(real_cpo.value));
        }
        case CPO::Type::Integer:{
            const CPOInteger& real_cpo = static_cast<const CPOInteger&>(cpo);
            return heap.addObject(*new RTOInteger(real_cpo.value));
        }
        default:
            throw InvalidBytecode("Requested to create an impossible run-time object from a constant pool object");
    }
}
void VM::runLiteral(void){
    fml_cp_index index = code_vector.getCPIndex(&ip);
    const CPO& cpo = constant_pool.getObject(index);
    fml_heap_index heap_index = CPO2RTO(cpo);
    operand_stack.push(heap_index);
    heap.release(heap_index);
}
void VM::printRTO(const RTO& rto){
    switch(rto.getType()){
        case RTO::Type::Null:
            std::cout << "null";
            break;
        case RTO::Type::Boolean:
            std::cout << (static_cast<const RTOBoolean&>(rto).value ? "true" : "false");
            break;
        case RTO::Type::Integer:
            std::cout << static_cast<const RTOInteger&>(rto).value;
            break;
    }
}
#define BACKLASH_ITEM(input, output) \
    case input: \
    std::cout << output; \
    break
void VM::runPrint(void){
    fml_cp_index format_index = code_vector.getCPIndex(&ip);
    uint8_t argument_count = code_vector.get8(&ip);
    const CPO& format = constant_pool.getObject(format_index);
    if(format.getType() != CPO::Type::String)
        throw InvalidBytecode("Print format object is not of type string");
    const CPOString& real_format = static_cast<const CPOString&>(format);
    std::vector<unsigned int> arguments;
    for(size_t i = 0; i < argument_count; i++)
        arguments.push_back(operand_stack.pop());
    std::reverse(arguments.begin(), arguments.end());

    // This whole block is largely taken from homework 2A
    const char *c_str = real_format.value.c_str();
    size_t i = 0;
    size_t curr_arg = 0;
    bool backlash_mode = false;
    for(char curr_char; (curr_char = c_str[i]) != '\0'; i++){
        if(backlash_mode){
            switch(curr_char){
                BACKLASH_ITEM('~', "~");
                BACKLASH_ITEM('n', std::endl);
                BACKLASH_ITEM('"', "\"");
                BACKLASH_ITEM('t', "\t");
                BACKLASH_ITEM('\\', "\\");
                case 'r':
                    break; // doesnt work in C++
                default:
                    throw InvalidBytecode("Unknown \\ operator");
            }
            backlash_mode = false;
        }else{
            if(curr_char == '\\')
                backlash_mode = true;
            else if(curr_char == '~'){
                const RTO& rto = heap.getObject(arguments[curr_arg]);
                printRTO(rto);
                heap.release(arguments[curr_arg]); // decrease RTO refcounter
                curr_arg++;
            }else
                std::cout << curr_char;
        }
    }
    if(backlash_mode)
        throw InvalidBytecode("Print cannot end with \\");

    // put the null RTO on stack
    fml_heap_index null = heap.addObject(*new RTONull);
    operand_stack.push(null);
    heap.release(null);
}
void VM::runDrop(void){
    heap.release(operand_stack.pop());
}
void VM::runGetGlobal(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    fml_heap_index heap_index = globals.getSlot(name_index);
    operand_stack.push(heap_index);
    heap.release(heap_index);
}
void VM::runSetGlobal(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    fml_heap_index heap_index = operand_stack.peek(0);
    globals.addSlot(name_index, heap_index);
    heap.release(heap_index);
}
void VM::runLabel(void){
    ip += 2; // labels are already processed when loading method CPOs
}
void VM::runJump(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    ip = labels.getLabel(name_index);
}
bool VM::evaluateTrue(const RTO& rto){
    switch(rto.getType()){
        case RTO::Type::Null:
            return false;
        case RTO::Type::Boolean:
            return static_cast<const RTOBoolean&>(rto).value;
        default:
            return true;
    }
}
void VM::runBranch(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    fml_heap_index condition_index = operand_stack.pop();
    const RTO& rto = heap.getObject(condition_index);
    if(evaluateTrue(rto))
        ip = labels.getLabel(name_index);
    heap.release(condition_index);
}
void VM::runCallFunction(void){
    fml_cp_index name_index = code_vector.getCPIndex(&ip);
    uint8_t argument_count = code_vector.get8(&ip);
    const CPO& cpo = constant_pool.getObject(globals.getMethod(name_index));
    if(cpo.getType() != CPO::Type::Method)
        throw InvalidBytecode("Calling something not a method");
    const CPOMethod& method = static_cast<const CPOMethod&>(cpo);
    if(argument_count != method.argument_count)
        throw InvalidBytecode("Function call's argument count not matching method definition");

    frame_stack.addFrame(*new Frame(ip, argument_count + method.local_count));
    for(size_t i = argument_count; i > 0; i--){
        fml_heap_index heap_index = operand_stack.pop();
        frame_stack.setLocal(i - 1, heap_index);
        heap.release(heap_index);
    }for(size_t i = argument_count; i < argument_count + method.local_count; i++){
        fml_heap_index null = heap.addObject(*new RTONull);
        frame_stack.setLocal(i, null);
        heap.release(null);
    }

    ip = method.start;
}
void VM::runReturn(void){
    ip = frame_stack.getReturnAddress();
    frame_stack.removeFrame();
}
void VM::runGetLocal(void){
    fml_local_index local_index = code_vector.getLocalIndex(&ip);
    fml_heap_index heap_index = frame_stack.getLocal(local_index);
    operand_stack.push(heap_index);
    heap.release(heap_index);
}
void VM::runSetLocal(void){
    fml_local_index local_index = code_vector.getLocalIndex(&ip);
    fml_heap_index heap_index = operand_stack.peek(0);
    frame_stack.setLocal(local_index, heap_index);
    heap.release(heap_index);
}
#define runIP_entry(arg) \
case Opcodes::arg: \
    run##arg(); \
    break;
void VM::runIP(void){
    while(ip < code_vector.size()){ // TODO: what is the real expected end of execution?
        uint8_t opcode = *code_vector.get(ip);
        ip++;
        switch(opcode){
            runIP_entry(Literal);
            runIP_entry(Print);
            runIP_entry(Drop);
            runIP_entry(GetGlobal);
            runIP_entry(SetGlobal);
            runIP_entry(Label);
            runIP_entry(Jump);
            runIP_entry(Branch);
            runIP_entry(CallFunction);
            runIP_entry(Return);
            runIP_entry(GetLocal);
            runIP_entry(SetLocal);
            default:
                throw InvalidBytecode("Unknown opcode: " + std::to_string(opcode));
        }
    }
}



VM::~VM(void){
    cleanup();
}
void VM::cleanup(void){
    operand_stack.cleanup();
    globals.cleanup();
    frame_stack.cleanup();
}



void VM::loadFile(std::ifstream& file){
    loadConstantPool(file);
    loadGlobals(file);
    loadEntry(file);
}
void VM::run(void){
    const CPOMethod& main = static_cast<const CPOMethod&>(constant_pool.getObject(entry));
    frame_stack.addFrame(*new Frame(0, main.local_count));
    ip = main.start;
    runIP();
    frame_stack.removeFrame();
    cleanup();
}
