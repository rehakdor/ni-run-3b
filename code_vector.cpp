#include "code_vector.hpp"

#include "util.hpp"

#include <cstdint>
#include <vector>
#include <cstring>

const uint8_t *CodeVector::get(fml_ip ip) const{
    return code.data() + ip;
}
uint8_t CodeVector::get8(fml_ip *ip) const{
    return *get((*ip)++);
}
uint16_t CodeVector::get16(fml_ip *ip) const{
    uint16_t result;
    std::memcpy(&result, get(*ip), sizeof(result));
    *ip += sizeof(result);
    return LE2NATIVE16(result);
}
uint32_t CodeVector::get32(fml_ip *ip) const{
    uint32_t result;
    std::memcpy(&result, get(*ip), sizeof(result));
    *ip += sizeof(result);
    return LE2NATIVE32(result);
}
fml_cp_index CodeVector::getCPIndex(fml_ip *ip) const{
    return get16(ip);
}
fml_local_index CodeVector::getLocalIndex(fml_ip *ip) const{
    return get16(ip);
}

void CodeVector::extend(size_t length){
    code.resize(code.size() + length);
}
uint8_t *CodeVector::getWritable(fml_ip ip){
    return code.data() + ip;
}

size_t CodeVector::size(void) const{
    return code.size();
}
