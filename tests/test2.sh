#!/bin/bash

for i in *.bc; do
    echo ====================
    echo "Running $i"
    ../main "$i"
    echo
done
