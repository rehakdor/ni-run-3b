#ifndef VM_H
#define VM_H

#include "constant_pool.hpp"
#include "heap.hpp"
#include "operand_stack.hpp"
#include "code_vector.hpp"
#include "frame_stack.hpp"
#include "globals.hpp"
#include "labels.hpp"
#include "types.hpp"

#include <fstream>
#include <cstdint>

class VM{
protected:
    // returns remaining size of an instruction after an opcode was read
    size_t opcodeArgSize(uint8_t opcode);
    // these methods create constant pool objects
    CPOInteger *loadCPOInteger(std::ifstream& file);
    CPOBoolean *loadCPOBoolean(std::ifstream& file);
    CPOString *loadCPOString(std::ifstream& file);
    CPOSlot *loadCPOSlot(std::ifstream& file);
    CPOMethod *loadCPOMethod(std::ifstream& file);
    CPO *loadCPO(std::ifstream& file);
    void loadConstantPool(std::ifstream& file);
    void loadGlobals(std::ifstream& file);
    void loadEntry(std::ifstream& file);

    void registerLabels(void);

    // creates a runtime object from a constant pool object, inserts it in heap, returns pointer to heap
    unsigned int CPO2RTO(const CPO& cpo);

    void runLiteral(void);
    void printRTO(const RTO& rto);
    void runPrint(void);
    void runDrop(void);
    void runGetGlobal(void);
    void runSetGlobal(void);
    void runLabel(void);
    void runJump(void);
    bool evaluateTrue(const RTO& rto);
    void runBranch(void);
    void runCallFunction(void);
    void runReturn(void);
    void runGetLocal(void);
    void runSetLocal(void);
    void runIP(void);

public:
    ConstantPool constant_pool;
    Heap heap;
    CodeVector code_vector;
    OperandStack operand_stack;
    Globals globals;
    Labels labels;
    FrameStack frame_stack;
    fml_cp_index entry;
    fml_ip ip;

    VM(void)
    : operand_stack(heap), globals(heap), frame_stack(heap){
    }
    ~VM(void);
    void cleanup(void);

    void loadFile(std::ifstream& file);
    void run(void);
};

#endif
