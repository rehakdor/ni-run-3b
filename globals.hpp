#ifndef GLOBALS_H
#define GLOBALS_H

#include "types.hpp"
#include "heap.hpp"

#include <map>

class Globals{
protected:
    Heap& heap;
    std::map<fml_cp_index, fml_heap_index> slots;
    std::map<fml_cp_index, fml_cp_index> methods;

public:
    Globals(Heap& pheap)
    : heap(pheap){
    }
    ~Globals(void);
    void cleanup(void);

    void addSlot(fml_cp_index name, fml_heap_index rto);
    void addMethod(fml_cp_index name, fml_cp_index method);
    fml_heap_index getSlot(fml_cp_index name);
    fml_cp_index getMethod(fml_cp_index name);
};

#endif
