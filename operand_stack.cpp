#include "operand_stack.hpp"

#include "heap.hpp"
#include "types.hpp"

#include <vector>

OperandStack::~OperandStack(void){
    cleanup();
}
void OperandStack::cleanup(void){
    for(fml_heap_index index : stack)
        heap.release(index);
    stack.clear();
}

fml_heap_index OperandStack::pop(void){
    fml_heap_index result = stack.back();
    stack.pop_back();
    return result;
}
void OperandStack::push(fml_heap_index pointer){
    stack.push_back(pointer);
    heap.take(pointer);
}
fml_heap_index OperandStack::peek(size_t index){
    fml_heap_index result = stack[stack.size() - index - 1];
    heap.take(result);
    return result;
}
size_t OperandStack::size(void){
    return stack.size();
}
