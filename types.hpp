#ifndef TYPES_H
#define TYPES_H

#include <cstdint>

using fml_int = int32_t;
using fml_cp_index = uint16_t;
using fml_heap_index = uint16_t;
#define FML_HEAP_INDEX_UNDEF (UINT16_MAX)
using fml_local_index = uint16_t;
using fml_ip = uint16_t;

#endif
