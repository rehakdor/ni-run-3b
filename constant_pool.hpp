#ifndef CONSTANT_POOL_H
#define CONSTANT_POOL_H

#include "types.hpp"

#include <cstdint>
#include <vector>
#include <string>

// Constant Pool Object base
class CPO{
public:
    enum class Type{
        Null,
        Boolean,
        Integer,
        String,
        Slot,
        Method
    };

    virtual ~CPO(void) = default;
    virtual Type getType(void) const = 0;
};
class CPONull: public CPO{
public:
    Type getType(void) const override;
};
class CPOBoolean: public CPO{
public:
    const bool value;

    CPOBoolean(bool pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOInteger: public CPO{
public:
    const fml_int value;

    CPOInteger(fml_int pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOString: public CPO{
public:
    const std::string value;

    CPOString(const std::string& pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class CPOSlot: public CPO{
public:
    const fml_cp_index index;

    CPOSlot(fml_cp_index pindex)
    : index(pindex){
    }
    Type getType(void) const override;
};
class CPOMethod: public CPO{
public:
    const fml_cp_index name_index;
    const uint8_t argument_count;
    const uint16_t local_count;
    const fml_ip start;
    const fml_ip length;

    CPOMethod(fml_cp_index pname_index, uint8_t pargument_count, uint16_t plocal_count,
              fml_ip pstart, fml_ip plength)
    : name_index(pname_index), argument_count(pargument_count), local_count(plocal_count),
      start(pstart), length(plength){
    }
    Type getType(void) const override;
};

class ConstantPool{
protected:
    std::vector<CPO *> objects;

public:
    ~ConstantPool(void);

    void addObject(CPO& object);
    const CPO& getObject(uint16_t index) const;
};

#endif
