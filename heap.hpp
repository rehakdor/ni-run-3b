#ifndef HEAP_H
#define HEAP_H

#include "types.hpp"

#include <map>

class Heap;

// Run-Time Object base
class RTO{
public:
    enum class Type{
        Null,
        Boolean,
        Integer
    };

    // reference counter
    unsigned int refcount = 1;
    void take(void);

    virtual ~RTO(void) = default;
    virtual Type getType(void) const = 0;
};
class RTONull: public RTO{
public:
    Type getType(void) const override;
};
class RTOBoolean: public RTO{
public:
    bool value;

    RTOBoolean(bool pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};
class RTOInteger: public RTO{
public:
    fml_int value;

    RTOInteger(fml_int pvalue)
    : value(pvalue){
    }
    Type getType(void) const override;
};

class Heap{
protected:
    fml_heap_index next_pointer = 0;
    std::map<fml_heap_index, RTO *> objects;

    void removeObject(fml_heap_index pointer);

public:
    RTO& getObject(fml_heap_index pointer);
    fml_heap_index addObject(RTO& new_object);

    void take(fml_heap_index pointer);
    void release(fml_heap_index pointer);

    // debug
    void print(void);
};

#endif
