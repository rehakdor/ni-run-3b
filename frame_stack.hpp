#ifndef FRAME_STACK_H
#define FRAME_STACK_H

#include "types.hpp"
#include "heap.hpp"

#include <vector>

class Frame{
public:
    const fml_ip return_address;
    std::vector<fml_heap_index> locals;

    Frame(fml_ip preturn_address, size_t locals_count)
    : return_address(preturn_address), locals(locals_count, FML_HEAP_INDEX_UNDEF){
    }

    void cleanup(Heap& heap);
};

class FrameStack{
protected:
    Heap& heap;
    std::vector<Frame *> frames;

    Frame& getFrame(void);
    const Frame& getFrame(void) const;

public:
    FrameStack(Heap& pheap)
    : heap(pheap){
    }
    ~FrameStack(void);
    void cleanup(void);

    void addFrame(Frame& frame);
    void removeFrame(void);

    fml_ip getReturnAddress(void) const;
    fml_heap_index getLocal(fml_local_index index) const;
    void setLocal(fml_local_index local_index, fml_heap_index heap_index);
};

#endif
