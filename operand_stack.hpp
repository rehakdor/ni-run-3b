#ifndef OPERAND_STACK_H
#define OPERAND_STACK_H

#include "heap.hpp"
#include "types.hpp"

#include <vector>

class OperandStack{
protected:
    Heap& heap;
    std::vector<fml_heap_index> stack;

public:
    OperandStack(Heap& pheap)
    : heap(pheap){
    }
    ~OperandStack(void);
    void cleanup(void);

    fml_heap_index pop(void);
    void push(fml_heap_index pointer);
    fml_heap_index peek(size_t index);
    size_t size(void);
};

#endif
