#include "frame_stack.hpp"

#include "types.hpp"

#include <vector>

void Frame::cleanup(Heap& heap){
    for(fml_heap_index index : locals)
        heap.release(index);
}

FrameStack::~FrameStack(void){
    cleanup();
}
void FrameStack::cleanup(void){
    while(!frames.empty())
        removeFrame();
}

void FrameStack::addFrame(Frame& frame){
    frames.push_back(&frame);
}
void FrameStack::removeFrame(void){
    Frame *frame = frames.back();
    frames.pop_back();
    frame->cleanup(heap);
    delete frame;
}
Frame& FrameStack::getFrame(void){
    return *frames.back();
}
const Frame& FrameStack::getFrame(void) const{
    return *frames.back();
}

fml_ip FrameStack::getReturnAddress(void) const{
    return getFrame().return_address;
}
fml_heap_index FrameStack::getLocal(fml_local_index index) const{
    fml_heap_index result = getFrame().locals[index];
    heap.take(result);
    return result;
}
void FrameStack::setLocal(fml_local_index local_index, fml_heap_index heap_index){
    fml_heap_index old = getFrame().locals[local_index];
    if(old != FML_HEAP_INDEX_UNDEF)
        heap.release(old);
    getFrame().locals[local_index] = heap_index;
    heap.take(heap_index);
}
