#ifndef CODE_VECTOR_H
#define CODE_VECTOR_H

#include "types.hpp"

#include <vector>
#include <cstdint>

// Holds all the code
class CodeVector{
protected:
    std::vector<uint8_t> code;

public:
    // this is used to read the code
    const uint8_t *get(fml_ip ip) const;
    // these automatically convert from little endian and advance IP
    uint8_t get8(fml_ip *ip) const;
    uint16_t get16(fml_ip *ip) const;
    uint32_t get32(fml_ip *ip) const;
    // bit size agnostic versions
    fml_cp_index getCPIndex(fml_ip *ip) const;
    fml_local_index getLocalIndex(fml_ip *ip) const;


    // this is used to append the code
    void extend(size_t length);
    uint8_t *getWritable(fml_ip ip);
    size_t size(void) const;
};

#endif
